const router = require('koa-router')()
const getData = require('../controllers/index')

router.get('/vaccineData',  getData)

module.exports = router