const {getDataFromHospitalCode} = require('../db/queries/getDataFromHCode')

const getData = async(ctx) =>{

  const hCode = ctx.request.query.hCode
  try{    
    const data = await getDataFromHospitalCode(+hCode)    
    ctx.status = 200  
    ctx.body = data

  }catch(e){
    console.log(e);
  }
}

module.exports = getData;